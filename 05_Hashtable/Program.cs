﻿using System.Collections;

namespace _05_Hashtable
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Membuat Hashtable baru
            Hashtable ibuKota = new Hashtable();

            // Menambahkan elemen pada Hashtable, key tidak boleh duplikasi, value boleh
            ibuKota.Add("Indonesia", "DKI Jakarta");
            ibuKota.Add("Thailand", "Bangkok");
            ibuKota.Add("Malaysia", "Kuala Lumpur");

            // Membuktikan bahwa key tidak boleh duplikasi
            try
            {
                ibuKota.Add("Indonesia", "Kalimantan");
            }
            catch
            {
                Console.WriteLine("Ibu kota Indonesia sudah ada.");
            }


            // Menampilkan value dari key "Indonesia"
            Console.WriteLine("Ibu kota Indonesia adalah " + ibuKota["Indonesia"]);

            // Dapat mengganti value dari key
            ibuKota["Indonesia"] = "Kalimantan";
            Console.WriteLine("Ibu kota Indonesia terbaru adalah " + ibuKota["Indonesia"]);

            // Kalau belum ada keynya, otomatis dibuat key baru
            ibuKota["Amerika"] = "Washington DC";

            Console.WriteLine();
            if (!ibuKota.ContainsKey("Australia"))
            {
                ibuKota.Add("Australia", "Canberra");
                Console.WriteLine("Menambahkan value " + ibuKota["Australia"] + " untuk key Australia.");
            }

            // Menggunakan foreach untuk enumerasi elemen hash table
            // Elemen diambil sebagai KeyValuePair objects
            Console.WriteLine();
            foreach(DictionaryEntry de in ibuKota)
            {
                Console.WriteLine("Key = {0}, Value = {1}", de.Key, de.Value);
            }

            // Hanya mengambil values saja
            ICollection valueColl = ibuKota.Values;

            Console.WriteLine();
            foreach(string s in valueColl)
            {
                Console.WriteLine("Value = {0}", s);
            }

            // Hanya mengambil keys saja
            ICollection keyColl = ibuKota.Keys;

            Console.WriteLine();
            foreach(string s in keyColl)
            {
                Console.WriteLine("Key = {0}", s);
            }

            // Menggunakan metode Remove untuk menghapus sebuah key/value pair
            Console.WriteLine("\nRemove Australia");
            ibuKota.Remove("Australia");

            // Membuktikan bahwa key Australia sudah terhapus
            if (!ibuKota.ContainsKey("Australia"))
            {
                Console.WriteLine("Key Australia is not found.");
            }
        }
    }
}